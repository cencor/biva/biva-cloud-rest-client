/**
 * 
 */
package com.cencor.biva;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @author gus
 *
 */
@Configuration
public class MyBivaCloudRestClientConfiguration {

    /**
     * @param restTemplateBuilder
     * @param connTimeout
     * @param readTimeout
     * @return
     */
    @Bean
    RestTemplate restTemplate(final RestTemplateBuilder restTemplateBuilder,
            final @Value("${rest.conn.timeout}") int connTimeout,
            final @Value("${rest.read.timeout}") int readTimeout) {
        return restTemplateBuilder
                .setConnectTimeout(Duration.ofMillis(connTimeout))
                .setReadTimeout(Duration.ofMillis(readTimeout))
                .build();
    }
}
