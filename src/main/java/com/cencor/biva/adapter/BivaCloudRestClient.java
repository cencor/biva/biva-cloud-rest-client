/**
 * 
 */
package com.cencor.biva.adapter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.cencor.biva.resource.BivaCloudHypermediaControls;
import com.cencor.biva.resource.InstrumentStats;

/**
 * @author gus
 *
 */
@Component
public class BivaCloudRestClient implements BivaCloudClient {

    /***/
    private static final Logger LOGGER = LoggerFactory.getLogger(BivaCloudRestClient.class);
    /***/
    private final String domain;
    /***/
    private final String apiKey;
    /***/
    private final RestTemplate restTemplate;
    
    /**
     * @param restTemplate
     * @param domain
     */
    @Autowired
    public BivaCloudRestClient(
            @Value("${bivacloud.domain}") final String domain, 
            @Value("${bivacloud.apikey}") final String apiKey, 
            final RestTemplate restTemplate) {
        this.domain = domain;
        this.apiKey = apiKey;
        this.restTemplate = restTemplate;
    }

    @Override
    public InstrumentStats getInstrumentStats(final String instrumentName) {
        HttpHeaders bivaCloudHeaders = new HttpHeaders();
        bivaCloudHeaders.set("x-api-key", apiKey);
        
        HttpEntity<Object> requestEntity = new HttpEntity<>(bivaCloudHeaders);
        
        String url = domain + "/stock-exchange/{stock-exchange}/instruments/stats?instrument={instrument}";
        
        String stockExchange = "BIVA";
        ResponseEntity<BivaCloudHypermediaControls> response = null;
        try {
            response = restTemplate.exchange(url,
                // Method
                HttpMethod.GET,
                // Request entity with headers.
                requestEntity,
                // Response Type
                BivaCloudHypermediaControls.class,
                // Path variable
                stockExchange, 
                // Query param
                instrumentName);
        } catch (HttpClientErrorException e) {
            // TODO: handle bad request exception
            LOGGER.error("Ocurrió un error al solicitar las estadísticas o el instrumento {} no existe.", 
                    instrumentName, e);
            /*
             * TODO Preferir el patron NullObjectPattern. Quizás sea más conveniente lanzar
             * una Runtime exception. Se devuelve null para efectos ilustrativos.
             */
            return null;
        } catch (HttpServerErrorException e) {
            // TODO: handle exception in case BIVA Cloud has an error or is not available.
            LOGGER.warn("Ocurrió un error del lado de BIVA Cloud...");
            /*
             * TODO Preferir el patron NullObjectPattern. Quizás sea más conveniente lanzar
             * una Runtime exception. Se devuelve null para efectos ilustrativos.
             */
            return null;
        }
        
        /*
         * Al buscar por nombre de instrumento se busca por nombre exacto. Por lo tanto
         * no puede haber más de un resultado.
         * 
         * En caso de no haber resultados provocaría una HttpClientErrorException que es
         * manejada anteriormente. Por lo tanto en este punto es seguro leer el dato que
         * solicitamos.
         */
        return response.getBody().getContent().get(0);
    }

}
