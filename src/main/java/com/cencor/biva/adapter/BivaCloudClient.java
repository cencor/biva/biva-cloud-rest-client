/**
 * 
 */
package com.cencor.biva.adapter;

import com.cencor.biva.resource.InstrumentStats;

/**
 * @author gus
 *
 */
public interface BivaCloudClient {

    InstrumentStats getInstrumentStats(String displayName);
}
