/**
 * 
 */
package com.cencor.biva.resource;

/**
 * @author gus
 *
 */
public class InstrumentStats {

    /***/
    private String instrument;
    /***/
    private String type;
    /***/
    private String volume;
    /***/
    private String amount;
    /***/
    private String trades;
    /***/
    private long lastTradeTime;
    /**
     * @return the instument
     */
    public final String getInstrument() {
        return instrument;
    }
    /**
     * @param instument the instument to set
     */
    public final void setInstument(final String instrument) {
        this.instrument = instrument;
    }
    /**
     * @return the type
     */
    public final String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public final void setType(final String type) {
        this.type = type;
    }
    /**
     * @return the volume
     */
    public final String getVolume() {
        return volume;
    }
    /**
     * @param volume the volume to set
     */
    public final void setVolume(final String volume) {
        this.volume = volume;
    }
    /**
     * @return the amount
     */
    public final String getAmount() {
        return amount;
    }
    /**
     * @param amount the amount to set
     */
    public final void setAmount(final String amount) {
        this.amount = amount;
    }
    /**
     * @return the trades
     */
    public final String getTrades() {
        return trades;
    }
    /**
     * @param trades the trades to set
     */
    public final void setTrades(final String trades) {
        this.trades = trades;
    }
    /**
     * @return the lastTradeTime
     */
    public final long getLastTradeTime() {
        return lastTradeTime;
    }
    /**
     * @param lastTradeTime the lastTradeTime to set
     */
    public final void setLastTradeTime(final long lastTradeTime) {
        this.lastTradeTime = lastTradeTime;
    }
}
