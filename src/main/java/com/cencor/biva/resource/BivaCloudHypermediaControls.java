/**
 * 
 */
package com.cencor.biva.resource;

import java.util.List;

/**
 * @author gus
 *
 */
public class BivaCloudHypermediaControls {

    /***/
    private List<InstrumentStats> content;

    /**
     * @return the content
     */
    public final List<InstrumentStats> getContent() {
        return content;
    }

    /**
     * @param content the content to set
     */
    public final void setContent(final List<InstrumentStats> content) {
        this.content = content;
    }
}
