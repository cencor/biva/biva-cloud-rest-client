/**
 * 
 */
package com.cencor.biva;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author gusvmx
 *
 */
@SpringBootApplication
@EnableSwagger2
public class MyBivaCloudRestClientApplication {

    /***/
    private static final Logger LOGGER = LoggerFactory.getLogger(MyBivaCloudRestClientApplication.class);
    
    /**
     * @param args
     */
    public static void main(final String[] args) {
        SpringApplication.run(MyBivaCloudRestClientApplication.class);
        LOGGER.info("Aplicacion inicializada");
    }

}
