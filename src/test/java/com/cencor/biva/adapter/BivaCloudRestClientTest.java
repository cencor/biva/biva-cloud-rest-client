/**
 * 
 */
package com.cencor.biva.adapter;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathEqualTo;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;

import com.cencor.biva.resource.InstrumentStats;
import com.github.tomakehurst.wiremock.matching.EqualToPattern;
import com.github.tomakehurst.wiremock.matching.StringValuePattern;

/**
 * @author gus
 *
 */
@SpringBootTest
@AutoConfigureWireMock(port = 0)
class BivaCloudRestClientTest {

    /***/
    @Autowired
    private BivaCloudClient bivaCloudClient;
    /***/
    @Value("${bivacloud.apikey}")
    private String apiKey;
    
    @Test
    void getInstrumentStats() throws IOException {
        String bodyMockResponse = new String(Files.readAllBytes(
                Paths.get("src/test/resources/bivacloudresponse/bivaCloudWalmexStatsResponse.json")));
        String targetPath = "/stock-exchange/BIVA/instruments/stats";
        Map<String, StringValuePattern> queryParams = new HashMap<>();
        queryParams.put("instrument", new EqualToPattern("WALMEX *"));
        stubFor(get(urlPathEqualTo(targetPath))
                .withQueryParams(queryParams)
                .withHeader("x-api-key", equalTo(apiKey))
                .willReturn(aResponse()
                        .withBody(bodyMockResponse)
                        .withStatus(200)
                        .withHeader("content-type", "application/json")));

        InstrumentStats walmexStats = bivaCloudClient.getInstrumentStats("WALMEX *");
        
        Assertions.assertEquals("WALMEX *", walmexStats.getInstrument());
        Assertions.assertEquals("48801531.4", walmexStats.getAmount());
        Assertions.assertEquals(1607741980095L, walmexStats.getLastTradeTime());
        Instant lastTradeTime = Instant.ofEpochMilli(walmexStats.getLastTradeTime());
        ZonedDateTime lastTradeTimeInMexicoCityTimeZone = lastTradeTime.atZone(ZoneId.of("America/Mexico_City"));
        Assertions.assertEquals(2020, lastTradeTimeInMexicoCityTimeZone.getYear());
        Assertions.assertEquals(12, lastTradeTimeInMexicoCityTimeZone.getMonthValue());
        Assertions.assertEquals(11, lastTradeTimeInMexicoCityTimeZone.getDayOfMonth());
        Assertions.assertEquals(20, lastTradeTimeInMexicoCityTimeZone.getHour());
        Assertions.assertEquals(59, lastTradeTimeInMexicoCityTimeZone.getMinute());
        Assertions.assertEquals(40, lastTradeTimeInMexicoCityTimeZone.getSecond());
        
        Assertions.assertEquals("5617", walmexStats.getTrades());
        Assertions.assertEquals("equity", walmexStats.getType());
        Assertions.assertEquals("857870", walmexStats.getVolume());
    }
}
